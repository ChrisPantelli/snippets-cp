import { useFetch } from "../api/useFetch.hook";
import { useCallback, useEffect, useState } from "react";
import style from "../components/Comment.module.css"

function Comments({ snippetId }) {
  const [makeRequest, { data }] = useFetch();

  const [endpoint] = useState(
    `/comments?snippetId=${snippetId}&_expand=author`,
  );

  const fetchComments = useCallback(
    () => makeRequest(endpoint),
    [makeRequest, endpoint],
  );

  useEffect(fetchComments, [fetchComments]);
  if(data) {
    return (
      <div>
        <h4 className="mdc-typography--headline6">Comments ({data.length}): </h4>
        {data.map((comment) => (
          <pre key={comment.id} className={style.CommentCard}>
            {comment.body} <br></br>
            - {comment.author.name}
          </pre>
        ))}
      </div>
    );
  }
}

export default Comments;
