import { Buffer } from "buffer";
import SyntaxHighlighter from "react-syntax-highlighter";
import { atomOneDark } from "react-syntax-highlighter/dist/esm/styles/hljs";
import Comment from "./Comment";
import IconTextButton from "./IconTextButton";
import LanguageBadge from "./LanguageBadge";
import styles from "./SnippetCard.module.css";
import { useUpdateSnippet } from "../api/useUpdateSnippet.hook";
import { useCallback, useState } from "react";


// Map language keys to their SyntaxHighlighter styles
// See https://github.com/react-syntax-highlighter/react-syntax-highlighter
const LanguageStyleMap = {
  js: "javascript",
  go: "go",
  py: "python",
};

// Try and decode the snippet from base64
function tryDecodeSnippet(raw) {
  try {
    return { decoded: Buffer.from(raw, "base64").toString(), err: null };
  } catch (err) {
    return { err: `Failed to parse Base64 encoded snippet: ${err.message}` };
  }
}

function SnippetCard(props) {
  const [data, setData] = useState(props);
  const [update] = useUpdateSnippet();
  const [voted, setVoted] = useState(false);

  // TODO: Implement upvote functionality
  const onClick = useCallback( () => {
      const newData = {...data, votes: data.votes + 1}
      delete newData.newData;
      setData(newData);
      update({ snippetId: data.id, body: newData, onUpdated: () => {
        setVoted(true);
      }});
    }, 
    [update, data]
  )

  // Default to plaintext if no supported language is supplied
  const syntax = LanguageStyleMap[data.language.key] || "plaintext";

  const { decoded, err } = tryDecodeSnippet(data.body);

  if (err) {
    // For now, don't render any broken snippets
    console.error(err);
    return null;
  }

  return (
    <div className={styles.Card}>
      <LanguageBadge language={data.language} />
      <header>
        <h2 className="mdc-typography--headline4">{data.title}</h2>
        <IconTextButton icon={voted ? "check" : "arrow-up"} label={voted ? "Voted" : "Upvote"} disabled={voted} onClick={onClick} />
      </header>
      <p className="mdc-typography--subtitle2">By {data.author.name}</p>
      <p className="mdc-typography--body2">Votes: {data.votes}</p>
      <p className="mdc-typography--body2">{data.description}</p>
      <SyntaxHighlighter language={syntax} style={atomOneDark}>
        {decoded}
      </SyntaxHighlighter>
      <Comment snippetId={data.id}/>
    </div>
  );
}

export default SnippetCard;
