import { useCallback, useEffect, useState } from "react";
import { useFetch } from "../api/useFetch.hook";
import styles from "./Snippets.module.css";
import SnippetCardContainer from "./SnippetCard.container";

function SnippetsContainer() {
  const [makeRequest, { data, error }] = useFetch();
  const [filter, setFilter] = useState("Highest");

  // See https://github.com/typicode/json-server#relationships
  // This expands the author and language objects so we
  // don't have to fetch them by id subsequently
  const [endpoint, setEndpoint] = useState(
    "/snippets?_expand=author&_expand=language&_sort=votes&_order=desc",
  );

  const sortVotes = useCallback( () => {
    if(filter === "Highest") {
      setEndpoint("/snippets?_expand=author&_expand=language&_sort=votes&_order=asc");
      setFilter("Lowest");
    } else if(filter === "Lowest") {
      setEndpoint("/snippets?_expand=author&_expand=language&_sort=votes&_order=desc");
      setFilter("Highest");
    }
  }, [filter]);
  

  // Initial fetch
  useEffect(() => {
    makeRequest(endpoint);
  }, [makeRequest, endpoint]);

  if (error) {
    // TODO: Display error nicely
    return (
      <div className={styles.Container}>
        <center><h2 className="mdc-typography--headline1">Error Connecting To API</h2></center>
      </div>
    )
  }

  if (data) {
    return (
      <section className={styles.Container}>
        <div className={styles.Center}>
          <select className={styles.Dropdown} onChange={sortVotes}>
            <option>{filter}</option>
            <option>---</option>
            <option>Highest</option>
            <option>Lowest</option>
          </select>
        </div>
        {data.map(snippet => (
          <SnippetCardContainer snippetId={snippet.id} key={snippet.id} />
        ))}
      </section>
    );
  }

  return null;
}

export default SnippetsContainer;
